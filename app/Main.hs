module Main where

import Control.Concurrent
import qualified Control.Exception as E
import Data.Foldable
import System.IO
import qualified Data.Text as T
import qualified Data.Text.IO as T.IO
import qualified Data.Text.Lazy as T.L
import qualified Data.Text.Lazy.IO as T.L.IO

main :: IO ()
main =
  do
    for_ [1..100] go
    getLine
    pure ()

go :: Integer -> IO ()
go n =
  do
    id <- forkIO (T.IO.readFile "test.txt" >>= T.IO.putStr)
    threadDelay 1
    killThread id
    pure ()

readFileS1 :: FilePath -> IO String
readFileS1 name = openFile name ReadMode >>= hGetContents

readFileS2 :: FilePath -> IO String
readFileS2 name = withFile name ReadMode hGetContents

readFileS3 :: FilePath -> IO String
readFileS3 name =
  E.mask $ \restore -> do
    h <- openFile name ReadMode
    restore (hGetContents h)

readFileT1 :: FilePath -> IO T.Text
readFileT1 name = openFile name ReadMode >>= T.IO.hGetContents

readFileT2 :: FilePath -> IO T.Text
readFileT2 name = withFile name ReadMode T.IO.hGetContents

readFileTL1 :: FilePath -> IO T.L.Text
readFileTL1 name = openFile name ReadMode >>= T.L.IO.hGetContents

readFileTL2 :: FilePath -> IO T.L.Text
readFileTL2 name = withFile name ReadMode T.L.IO.hGetContents

readFileTL3 :: FilePath -> IO T.L.Text
readFileTL3 name =
  E.mask $ \restore -> do
    h <- openFile name ReadMode
    restore (T.L.IO.hGetContents h)

